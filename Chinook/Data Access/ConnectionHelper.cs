﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook
{
    /// <summary>
    /// Handles SQL connections.
    /// </summary>
    public class ConnectionHelper
    {
        const string ServerName = "PC1677\\SQLEXPRESS"; // CHANGE THIS

        /// <summary>
        /// Creates a connection to the Chinook database hosted on ServerName.
        /// </summary>
        /// <returns>A connection string.</returns>
        public static string GetConnectionString()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = ServerName; 
            builder.InitialCatalog = "Chinook";
            builder.IntegratedSecurity = true;
            builder.TrustServerCertificate = true;

            return builder.ConnectionString;
        }
    }
}
