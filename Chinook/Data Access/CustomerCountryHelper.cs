﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook
{
    /// <summary>
    /// Writes customers to a list and returns number of customers for each country in descending order
    /// </summary>
    public class CustomerCountryHelper : ICustomerCountryRepository
    {
        /// <summary>
        /// Method that creates a list of countries and number of custommers in the country
        /// </summary>
        /// <returns>List of custommers in country</returns>
        public List<CustomerCountry> CustomersByCountry()
        {
            List < CustomerCountry > customerCountries = new  List<CustomerCountry>();

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    string sql = "SELECT Country, COUNT(CustomerId) AS numberOffCustomers FROM Customer GROUP BY Country ORDER BY numberOffCustomers DESC";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerCountry customerCountry = new CustomerCountry()
                                {
                                    Count = reader.GetInt32(1),
                                    Country = reader.GetString(0),

                                };
                                customerCountries.Add(customerCountry);
                            }
                        }
                    }
                }

            }
            catch (SqlException)
            {
                Console.WriteLine("Something went wrong in the db.");

            }
            catch (InvalidCastException)
            {
                Console.WriteLine("Something was null.");

            }
            catch (Exception e)
            {
                Console.WriteLine("something else went wrong.");
            }
            return customerCountries;
        }
    }
}
