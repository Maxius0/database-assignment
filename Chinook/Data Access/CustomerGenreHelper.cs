﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook
{
    /// <summary>
    /// Handles genre-related reads from the database.
    /// </summary>
    public class CustomerGenreHelper : ICustomerGenreRepository
    {
        /// <summary>
        /// Given a customer id, determines the customer's favorite genre(s) by queryieng the db for most bought genre(s) and returns it/them.
        /// </summary>
        /// <param name="id">The id of the customer whose favorite genre to get.</param>
        /// <returns>A list of CustomerGenres containing the customer's favorite genre(s).</returns>
        public List<CustomerGenre> GetFavoriteGenre(int id)
        {
            List<CustomerGenre> customerGenres = new List<CustomerGenre>();

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    string sql = "WITH t1 AS( SELECT Genre.Name, COUNT(Genre.GenreId) AS GenreCount " +
                                 "FROM Invoice JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId " +
                                 "JOIN Track ON InvoiceLine.TrackId = Track.TrackId " +
                                 "JOIN Genre ON Track.GenreId = Genre.GenreId WHERE CustomerId = @id GROUP BY Genre.Name) " +
                                 "SELECT Name FROM t1 WHERE GenreCount = (SELECT MAX (GenreCount) FROM t1);";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id", id);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerGenre customerGenre = new CustomerGenre()
                                {
                                    GenreName = reader.GetString(0)
                                };
                                customerGenres.Add(customerGenre);
                            }
                        }
                    }
                }

            }
            catch (SqlException)
            {
                Console.WriteLine("Something went wrong in the db.");

            }
            catch (InvalidCastException)
            {
                Console.WriteLine("Something was null.");

            }
            catch (Exception e)
            {
                Console.WriteLine("something else went wrong.");
            }
            return customerGenres;
        }
    }
}
