﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook
{
    /// <summary>
    /// Reads Customers from a database and contains specific methods for data handling
    /// </summary>
    public class CustomerHelper : ICustomerRepository
    {
        /// <summary>
        /// Reads all customers from a table in a database
        /// </summary>
        /// <returns>A list of customers with selected parametres of: CustomerId, FirstName, LastName, Counrty, Email </returns>
        public List<Customer> ReadAllCustomers()
        {
            List<Customer> customers = new List<Customer>();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    string sql = "SELECT * FROM dbo.Customer";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer customer = new Customer() {
                                    CustomerId = reader.GetInt32(0), FirstName = reader.GetString(1), LastName = reader.GetString(2),
                                    Country = reader.GetString(7),                                 
                                    Email = reader.GetString(11)
                                };
                                if (!(reader.GetValue(8) is DBNull))
                                {
                                    customer.PostalCode = reader.GetString(8);
                                }
                                if (!(reader.GetValue(9) is DBNull))
                                {
                                    customer.Phone = reader.GetString(9);
                                }
                                customers.Add(customer);                             
                            }
                        }
                    }
                }

            }
            catch (SqlException)
            {
                Console.WriteLine("Something went wrong in the db.");

            }
            catch (InvalidCastException)
            {
                Console.WriteLine("Something was null.");

            }
            catch (Exception e)
            {
                Console.WriteLine("something else went wrong.");
            }

            return customers;
        }

        /// <summary>
        /// Find a specific customer by Id and print out the costumers: CustomerId, FirstName, LastName, Counrty, Email
        /// </summary>
        /// <param name="id">The id of the customer to get.</param>
        /// <returns>CustomerId, FirstName, LastName, Counrty, Email</returns>
        public Customer GetCustomerById(int id)
        {
            Customer customer = new Customer();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    string sql = "SELECT * FROM dbo.Customer WHERE CustomerId = @id";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id", id);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer = new Customer()
                                {
                                    CustomerId = reader.GetInt32(0),
                                    FirstName = reader.GetString(1),
                                    LastName = reader.GetString(2),
                                    Country = reader.GetString(7),
                                    Email = reader.GetString(11)
                                };
                                if (!(reader.GetValue(8) is DBNull))
                                {
                                    customer.PostalCode = reader.GetString(8);
                                }
                                if (!(reader.GetValue(9) is DBNull))
                                {
                                    customer.Phone = reader.GetString(9);
                                }
                            }
                        }
                    }
                }
            }
            catch (SqlException)
            {
                Console.WriteLine("Something went wrong in the db");

            }
            catch (InvalidCastException)
            {
                Console.WriteLine("Something was null");

            }
            catch (Exception e)
            {
                Console.WriteLine("Something else went wrong.");
            }
            return customer;
        }

        /// <summary>
        /// Finds a customer by string name. Both first and last name can be given. Several Customers with similar names can appear
        /// </summary>
        /// <param name="name"></param>
        /// <returns>CustomerId, FirstName, LastName, Counrty, Email</returns>
        public List<Customer> ReadCustomersByName(string name)
        {
            string searchWord = $"%{name}%";
            List<Customer> customers = new List<Customer>();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    string sql = "SELECT * FROM dbo.Customer WHERE (FirstName LIKE @searchWord) OR (LastName LIKE @searchWord)";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@searchWord", searchWord);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer customer = new Customer()
                                {
                                    CustomerId = reader.GetInt32(0),
                                    FirstName = reader.GetString(1),
                                    LastName = reader.GetString(2),
                                    Country = reader.GetString(7),
                                    Email = reader.GetString(11)
                                };
                                if (!(reader.GetValue(8) is DBNull))
                                {
                                    customer.PostalCode = reader.GetString(8);
                                }
                                if (!(reader.GetValue(9) is DBNull))
                                {
                                    customer.Phone = reader.GetString(9);
                                }
                                customers.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (SqlException)
            {
                Console.WriteLine("Something went wrong in the db");

            }
            catch (InvalidCastException)
            {
                Console.WriteLine("Something was null");

            }
            catch (Exception e)
            {
                Console.WriteLine("Something else went wrong.");
            }
            return customers;
        }

        /// <summary>
        /// Creates a list of customers from a given offset with a limited list number.
        /// </summary>
        /// <param name="offset">The amount of rows to skip.</param>
        /// <param name="limit">The amount of rows to get.</param>
        /// <returns>Limited list of customers with: CustomerId, FirstName, LastName, Counrty, Email.</returns>
        public List<Customer> ReadLimitedCustomers(int offset, int limit)
        {
            List<Customer> customers = new List<Customer>();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    string sql = "SELECT * FROM dbo.Customer ORDER BY CustomerId OFFSET @offset ROWS FETCH NEXT @limit ROWS ONLY";


                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@offset", offset);
                        command.Parameters.AddWithValue("@limit", limit);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer customer = new Customer()
                                {
                                    CustomerId = reader.GetInt32(0),
                                    FirstName = reader.GetString(1),
                                    LastName = reader.GetString(2),
                                    Country = reader.GetString(7),
                                    Email = reader.GetString(11)
                                };
                                if (!(reader.GetValue(8) is DBNull))
                                {
                                    customer.PostalCode = reader.GetString(8);
                                }
                                if (!(reader.GetValue(9) is DBNull))
                                {
                                    customer.Phone = reader.GetString(9);
                                }
                                customers.Add(customer);
                            }
                        }
                    }
                }

            }
            catch (SqlException)
            {
                Console.WriteLine("Something went wrong in the db.");

            }
            catch (InvalidCastException)
            {
                Console.WriteLine("Something was null.");

            }
            catch (Exception e)
            {
                Console.WriteLine("something else went wrong.");
            }

            return customers;
        }
        /// <summary>
        /// Add a customer to Customer table in a database.
        /// </summary>
        /// <param name="customer">The customer to add to the db.</param>
        /// <returns>'true' if the operation was successful; 'false' otherwise.</returns>
        public bool AddCustomer(Customer customer) 
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    string sql = "INSERT INTO dbo.Customer (FirstName, LastName, Country, PostalCode, Phone, Email) VALUES (@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        command.Parameters.AddWithValue("@LastName", customer.LastName);
                        command.Parameters.AddWithValue("@Country", customer.Country);
                        command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        command.Parameters.AddWithValue("@Phone", customer.Phone);
                        command.Parameters.AddWithValue("@Email", customer.Email);
                        command.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch (SqlException)
            {
                Console.WriteLine("Something went wrong in the db");

            }
            catch (Exception e)
            {
                Console.WriteLine("Something else went wrong.");
            }
            return false;
        }

        /// <summary>
        /// Update a customer in Customer table
        /// </summary>
        /// <param name="customer">The customer object containing the new data.</param>
        /// <param name="id">The id of the customer to update.</param>
        /// <returns>'true' if the operation was successful; 'false' otherwise.</returns>
        public bool UpdateCustomer(Customer customer, int id) 
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    string sql = "UPDATE DBO.Customer SET FirstName = @FirstName, LastName = @LastName, Country = @Country, PostalCode = @PostalCode, Phone = @Phone, Email = @Email WHERE CustomerId = @id";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id", id);
                        command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        command.Parameters.AddWithValue("@LastName", customer.LastName);
                        command.Parameters.AddWithValue("@Country", customer.Country);
                        command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        command.Parameters.AddWithValue("@Phone", customer.Phone);
                        command.Parameters.AddWithValue("@Email", customer.Email);
                        command.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch (SqlException)
            {
                Console.WriteLine("Something went wrong in the db");

            }
            catch (Exception e)
            {
                Console.WriteLine("Something else went wrong.");
            }
            return false;
        }
    }
}
