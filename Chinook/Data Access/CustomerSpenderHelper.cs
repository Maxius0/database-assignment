﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook
{
    /// <summary>
    /// Handles amount spent-related reads from the database.
    /// </summary>
    public class CustomerSpenderHelper : ICustomerSpenderRepository
    {
        /// <summary>
        /// Queries the db to get all customers that have spent money, and orders them by their total amount spent.
        /// </summary>
        /// <returns>A list of CustomerSpenders containing customers' id and their respective amount spent.</returns>
        public List<CustomerSpender> GetHighestSpenders()
        {
            List<CustomerSpender> customerSpenders = new List<CustomerSpender>();

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    string sql = "SELECT Customer.CustomerId, SUM(Total) AS TotalSpent " +
                                 "FROM Customer JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId " +
                                 "GROUP BY Customer.CustomerId ORDER BY TotalSpent DESC";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerSpender customerSpender = new CustomerSpender();
                                customerSpender.CustomerId = reader.GetInt32(0);
                                if (!(reader.GetValue(1) is DBNull))
                                {
                                    customerSpender.TotalSpent = (double) reader.GetDecimal(1);
                                }
                                customerSpenders.Add(customerSpender);
                            }
                        }
                    }
                }

            }
            catch (SqlException)
            {
                Console.WriteLine("Something went wrong in the db.");

            }
            catch (InvalidCastException)
            {
                Console.WriteLine("Something was null.");

            }
            catch (Exception e)
            {
                Console.WriteLine("something else went wrong.");
            }
            return customerSpenders;
        }
    }
}
