﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook
{
    /// <summary>
    /// Interface that specifies what CustomerCountryHelper must implement.
    /// </summary>
    public interface ICustomerCountryRepository
    {
        public List<CustomerCountry> CustomersByCountry();
    }
}
