﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook
{
    /// <summary>
    /// Interface that specifies what CustomerGenreHelper must implement.
    /// </summary>
    public interface ICustomerGenreRepository
    {
        List<CustomerGenre> GetFavoriteGenre(int id);
    }
}
