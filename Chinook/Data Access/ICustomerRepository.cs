﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook
{
    /// <summary>
    /// Interface that specifies what CustomerHelper must implement.
    /// </summary>
    public interface ICustomerRepository
    {

        bool AddCustomer(Customer customer);

        List<Customer> ReadAllCustomers();

        Customer GetCustomerById(int id);

        public List<Customer> ReadCustomersByName(string name);

        public List<Customer> ReadLimitedCustomers(int offset, int limit);

        bool UpdateCustomer(Customer customer, int id);
    }
}

