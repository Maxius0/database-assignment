﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook
{
    /// <summary>
    /// Interface that specifies what CustomerSpenderHelper must implement.
    /// </summary>
    public interface ICustomerSpenderRepository
    {
        public List<CustomerSpender> GetHighestSpenders();
    }
}
