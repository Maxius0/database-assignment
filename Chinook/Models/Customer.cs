﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook
{
    /// <summary>
    /// Contains properties for all the relevant columns of the Customer table in the db.
    /// </summary>
    public class Customer
    {
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Country { get; set; }

        public string PostalCode { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public override string ToString()
        {
            return $"{CustomerId} | {FirstName} | {LastName} | {Country} | {PostalCode} | {Phone} | {Email}";
        }
    }
}
