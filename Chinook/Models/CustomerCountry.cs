﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook
{
    /// <summary>
    /// A CustomerCountry has a country and a count representing the amount of customers from that country.
    /// </summary>
    public class CustomerCountry
    {
        public string Country { get; set; }
        public int Count { get; set; }

        public override string ToString()
        {
            return $"{Country} | {Count}";
        }
    }


}
