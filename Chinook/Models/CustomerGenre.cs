﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook
{
    /// <summary>
    /// A CustomerGenre is just a genre name.
    /// </summary>
    public class CustomerGenre
    {
        public string GenreName { get; set; }

        public override string ToString()
        {
            return GenreName;
        }
    }
}
