﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook
{
    /// <summary>
    /// A CustomerSpender has a customer ID and a double value representing the total amount of money the customer has spent.
    /// </summary>
    public class CustomerSpender
    {
        public int CustomerId { get; set; } 
        public double TotalSpent { get; set; }

        public override string ToString()
        {
            return $"{CustomerId} | {TotalSpent}";
        }
    }
}
