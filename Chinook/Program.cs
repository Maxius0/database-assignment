﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Chinook
{
    /// <summary>
    /// Contains the Main method and all test methods.
    /// </summary>
    class Program
    {
        /// <summary>
        /// Program execution starts here. The Main method runs all tests.
        /// </summary>
        static void Main()
        {
            Customer testCustomer = new Customer() { FirstName = "Kirsten", LastName = "Kirstensen",
                                                     Country = "Denmark", PostalCode = "4500", Phone = "20208080",
                                                     Email = "kirsten.kirstensen@kirstenmail.dk" };
            ReadAllCustomersTest();
            Console.WriteLine();
            GetCustomerByIdTest(42);
            Console.WriteLine();
            ReadCustomersByNameTest("Robert");
            Console.WriteLine();
            ReadLimitedCustomersTest(3, 5);
            Console.WriteLine();
            AddCustomerTest(testCustomer);
            Console.WriteLine();
            UpdateCustomerTest(testCustomer, 60);
            Console.WriteLine();
            CustomersByCountryTest();
            Console.WriteLine();
            GetHighestSpendersTest();
            Console.WriteLine();
            GetFavoriteGenreTest(6);
        }

        private static void PrintCustomerTable()
        {
            Console.WriteLine("ID | First name | Last name | Country | Postal code | Phone number | Email");
            Console.WriteLine("--------------------------------------------------------------------------");
        }

        private static void PrintCustomerCountryTable()
        {
            Console.WriteLine("Country | Number of customers");
            Console.WriteLine("-----------------------------");
        }

        private static void PrintCustomerSpenderTable()
        {
            Console.WriteLine("Customer ID | Total amount spent");
            Console.WriteLine("-----------------------------");
        }

        private static void PrintCustomerGenreTable()
        {
            Console.WriteLine("Favorite genre");
            Console.WriteLine("--------------");
        }

        static void ReadAllCustomersTest()
        {
            List<Customer> customers;
            CustomerHelper customerHelper = new CustomerHelper();

            Console.WriteLine("Printing all customers...\n");
            PrintCustomerTable();

            customers = customerHelper.ReadAllCustomers();
            foreach (Customer customer in customers)
            {
                Console.WriteLine(customer.ToString());
            }
        }

        static void GetCustomerByIdTest(int id)
        {
            Customer customer;
            CustomerHelper customerHelper = new CustomerHelper();

            Console.WriteLine($"Printing customer with id: {id}...\n");
            PrintCustomerTable();

            customer = customerHelper.GetCustomerById(id);
            Console.WriteLine(customer.ToString());
        }

        static void ReadCustomersByNameTest(string name)
        {
            List<Customer> customers;
            CustomerHelper customerHelper = new CustomerHelper();

            Console.WriteLine($"Printing customers with a name like: {name}...\n");
            PrintCustomerTable();

            customers = customerHelper.ReadCustomersByName(name);
            foreach (Customer customer in customers)
            {
                Console.WriteLine(customer.ToString());
            }
        }

        static void ReadLimitedCustomersTest(int offset, int limit)
        {
            List<Customer> customers;
            CustomerHelper customerHelper = new CustomerHelper();

            Console.WriteLine($"Printing {limit} customers, skipping the first {offset}...\n");
            PrintCustomerTable();

            customers = customerHelper.ReadLimitedCustomers(offset, limit);
            foreach (Customer customer in customers)
            {
                Console.WriteLine(customer.ToString());
            }
        }

        static void AddCustomerTest(Customer customer)
        {
            CustomerHelper customerHelper = new CustomerHelper();

            Console.WriteLine("Adding customer to database...\n");
          
            if (customerHelper.AddCustomer(customer))
            {
                Console.WriteLine("Customer successfully added!");
            }
            else
            {
                Console.WriteLine("Adding customer failed!");
            }
        }

        static void UpdateCustomerTest(Customer customer, int id)
        {
            CustomerHelper customerHelper = new CustomerHelper();

            Console.WriteLine($"Updating customer with id: {id}...\n");

            if (customerHelper.UpdateCustomer(customer, id))
            {
                Console.WriteLine("Customer successfully updated!");
            }
            else
            {
                Console.WriteLine("Updating customer failed!");
            }
        }

        static void CustomersByCountryTest()
        {
            CustomerCountryHelper customerCountryHelper = new CustomerCountryHelper();
            List<CustomerCountry> customerCountries;

            Console.WriteLine("Printing number of customers from each country in descending order...\n");         
            PrintCustomerCountryTable();

            customerCountries = customerCountryHelper.CustomersByCountry();
            foreach (CustomerCountry customerCountry in customerCountries)
            {
                Console.WriteLine(customerCountry.ToString());
            }
        }

        static void GetHighestSpendersTest()
        {
            CustomerSpenderHelper customerSpenderHelper = new CustomerSpenderHelper();
            List<CustomerSpender> customerCountries;

            Console.WriteLine("Printing all customers ordered by total amount spent in descending order...\n");
            PrintCustomerSpenderTable();

            customerCountries = customerSpenderHelper.GetHighestSpenders();
            foreach (CustomerSpender customerSpender in customerCountries)
            {
                Console.WriteLine(customerSpender.ToString());
            }
        }

        static void GetFavoriteGenreTest(int id)
        {
            CustomerGenreHelper customerGenreHelper = new CustomerGenreHelper();
            List<CustomerGenre> customerCountries;

            Console.WriteLine($"Printing favorite genres of customer with id: {id}...\n");
            PrintCustomerGenreTable();

            customerCountries = customerGenreHelper.GetFavoriteGenre(id);
            foreach (CustomerGenre customerGenre in customerCountries)
            {
                Console.WriteLine(customerGenre.ToString());
            }
        }
    }
}
