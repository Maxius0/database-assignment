# Data access with SQL Client

This is the second assignment in the backend module of Noroff's full stack development course. See assignment description [here](https://gitlab.com/Maxius0/database-assignment/-/blob/main/Project/Assignment_2_CSharp_Data_access_with_SQL_Client.pdf).

## Install

Clone or download repository.

## Usage

### Part 1: Superhero Database

Run all SQL scripts in order and observe their results through SSMS. A database diagram can be found [here](https://gitlab.com/Maxius0/database-assignment/-/blob/main/Project/Superheroes_diagram.png).

### Part 2: Chinook Database

1. Open solution in Visual Studio
2. IMPORTANT: Change value of the constant `ServerName` inside `ConnectionHelper.cs` to the name of the server you're hosting the Chinook database on.
3. Build & run solution.

## Maintainers

Marius Samson
https://gitlab.com/Maxius0

Jesper Jensen
https://gitlab.com/JesperArne
