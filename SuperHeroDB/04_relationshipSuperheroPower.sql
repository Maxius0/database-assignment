CREATE TABLE SuperHeroPower(
    Superhero_ID int REFERENCES Superhero(ID),
    Power_ID int REFERENCES Power(ID),
    PRIMARY KEY (Superhero_ID, Power_ID)
);