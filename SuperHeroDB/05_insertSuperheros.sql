INSERT INTO
    Superhero(
        Name,
        Alias,
        Origin
    )
VALUES
    (
        'Bobby',
        'Bob Logan',
        'Born from from death'
    ),
    (
        'Pingvin',
        'Signe',
        'Born from from ice'
    ),
    (
        'Chilli',
        'Franz',
        'Born from from fire'
    );