INSERT INTO
    Power(Name, Description)
VALUES
    ('Death', 'Makes people die'),
    ('Freeze', 'Makes people cold'),
    ('Fire', 'Makes people hot'),
    ('Genreic', 'Very Generic Power');

INSERT INTO
    SuperheroPower(Superhero_ID, Power_ID)
VALUES
    (1, 1),
    (1, 4),
    (2, 2),
    (2, 4),
    (3, 3),
    (3, 4);